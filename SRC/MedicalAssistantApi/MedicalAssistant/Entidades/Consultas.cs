﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalAssistant.Entidades
{
    public class Consultas
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public int IdPaciente { get; set; }
        public string MotivoConsulta { get; set; }
        public string Antecedentes { get; set; }
        public string Diagnostico { get; set; }
        public string Tratamiento { get; set; }
        public DateTime FechaConsulta { get; set; }
        public DateTime FechaProximaCita { get; set; }

        public virtual Pacientes Pacientes { get; set; }
    }
}
