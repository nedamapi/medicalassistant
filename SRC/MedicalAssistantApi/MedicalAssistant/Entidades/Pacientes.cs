﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalAssistant.Entidades
{
    public class Pacientes
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public int SeguroID { get; set; }
        public int SexoID { get; set; }
        public string Nombres{ get; set; }
        public string Apellidos { get; set; }
        public Int64 Cedula { get; set; }
        public string NumeroSeguro { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public int Edad { get; set; }
        public string Telefono { get; set; }
        public string Correo { get; set; }
        public string IdUsuarioCrea { get; set; }
        public string IdUsuarioModifica { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }
        public bool Estado { get; set; }

        public virtual Sexos Sexos { get; set; }
        public virtual Seguros Seguros { get; set; }
        public virtual ICollection<Consultas> Consultas { get; set; }

    }
}
