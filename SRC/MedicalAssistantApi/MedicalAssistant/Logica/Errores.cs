﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalAssistant.Logica
{
    public class Errores
    {
        public static string MessageExcepcion(Exception ex)
        {
            string messagee = string.Empty;

            if (ex.InnerException != null)
                messagee = ex.InnerException.Message;
            else
                messagee = ex.Message;

            return messagee;
        }

    }
}
