﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalAssistant.ViewModels.Pacientes
{
    public class UpdatePacienteVM
    {
        public int ID { get; set; }
        public int SeguroID { get; set; }
        public int SexoID { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public Int64 Cedula { get; set; }
        public string NumeroSeguro { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public string Telefono { get; set; }
        public string Correo { get; set; }
        public string IdUsuarioModifica { get; set; }
        public DateTime FechaModificacion { get; set; }
    }
}
