﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalAssistant.ViewModels.Pacientes
{
    public class InsertPacienteVM
    {
        public int ID { get; set; }
        public int SegurosID { get; set; }
        public int SexosID { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public Int64 Cedula { get; set; }
        public string NumeroSeguro { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public int Edad { get; set; }
        public string Telefono { get; set; }
        public string Correo { get; set; }
        public string IdUsuarioCrea { get; set; }
        public DateTime FechaCreacion { get; set; }
        public bool Estado { get; set; }
    }
}
