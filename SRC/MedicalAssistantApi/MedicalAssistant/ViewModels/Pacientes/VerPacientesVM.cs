﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalAssistant.ViewModels.Pacientes
{
    public class VerPacientesVM
    {
        public int ID { get; set; }
        public string Seguro { get; set; }
        public string Sexo { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public Int64 Cedula { get; set; }
        public string NumeroSeguro { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public int Edad { get; set; }
        public string Telefono { get; set; }
        public string Correo { get; set; }
        public string IdUsuarioCrea { get; set; }
        public string IdUsuarioModifica { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }
        public bool Estado { get; set; }

    }
}
