﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalAssistant.ViewModels.Consultas
{
    public class UpdateConsultaVM
    {
        public int ID { get; set; }
        public string MotivoConsulta { get; set; }
        public string Antecedentes { get; set; }
        public string Diagnostico { get; set; }
        public string Tratamiento { get; set; }
        public DateTime FechaConsulta { get; set; }
        public DateTime FechaProximaCita { get; set; }
    }
}
