﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalAssistant.Contracts
{
    public interface ILoggerServicecs
    {
        void logInfo(string message);
        void logWarn(string message);
        void logError(string message);
    }
}
