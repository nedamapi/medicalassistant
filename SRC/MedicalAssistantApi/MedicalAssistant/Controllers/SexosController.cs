﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MedicalAssistant.Datos;
using MedicalAssistant.Entidades;
using MedicalAssistant.ViewModels.Sexos;
using MedicalAssistant.Contracts;
using Microsoft.VisualStudio.Web.CodeGeneration.Contracts.Messaging;
using MedicalAssistant.Logica;
using NLog;

namespace MedicalAssistant.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SexosController : ControllerBase
    {
        private readonly MedicalAssistantDbContext _context;
        private readonly ILoggerServicecs _logger;

        public SexosController(MedicalAssistantDbContext context, ILoggerServicecs logger)
        {
            _context = context;
            _logger = logger;
        }

        // GET: api/Sexos
        [HttpGet]
        public async Task<IEnumerable<VerSexosVM>> GetSexos()
        {
            var sexos = await _context.Sexos.ToListAsync();

            return sexos.Select(s=> new VerSexosVM
            { 
                ID = s.ID,
                Descripcion = s.Descripcion
            });
        }

        // GET: api/Sexos/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetSexo([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var sexos = await _context.Sexos.FindAsync(id);

            if (sexos == null)
            {
                return NotFound();
            }

            return Ok(new VerSexosVM
            {
                ID = sexos.ID,
                Descripcion = sexos.Descripcion
            }) ;
        }

        // PUT: api/Sexos/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSexos([FromRoute] int id, [FromBody]  VerSexosVM model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if(model == null)
            {
                return BadRequest();
            }
            var sexo = await _context.Sexos.SingleOrDefaultAsync(s=>s.ID == id);

            sexo.Descripcion = model.Descripcion;

            try
            {
                await _context.SaveChangesAsync();
                _logger.logInfo("Sexo actualizado correctamente");
                _context.Entry(sexo).State = EntityState.Modified;
            }
            catch (DbUpdateConcurrencyException ex)
            {
                if (!SexosExists(id))
                {
                    _logger.logWarn("No se ha encontrado el sexo a actualizar.");
                    return NotFound();
                }
                else
                {
                    _logger.logError(Errores.MessageExcepcion(ex));
                    return BadRequest();
                }
            }

            return Ok();
        }

        // POST: api/Sexos
        [HttpPost]
        public async Task<IActionResult> InsertSexo([FromBody] VerSexosVM model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Sexos sexos = new Sexos()
            {
                Descripcion = model.Descripcion
            };

            _context.Sexos.Add(sexos);
            try
            {
                await _context.SaveChangesAsync();
                _logger.logInfo("Sexo insertado correctamente.");
            }
            catch (Exception ex)
            {
                _logger.logError(Errores.MessageExcepcion(ex));
                return BadRequest();
            }

            return Ok();
        }

        // DELETE: api/Sexos/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSexos([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var sexos = await _context.Sexos.FindAsync(id);
            if (sexos == null)
            {
                return NotFound();
            }

            _context.Sexos.Remove(sexos);

            try
            {
                await _context.SaveChangesAsync();
                _logger.logInfo("Sexo eliminado correctamente");
            }
            catch (Exception ex)
            {
                _logger.logError(Errores.MessageExcepcion(ex));
                return BadRequest();
            }

            return Ok(sexos);
        }

        private bool SexosExists(int id)
        {
            return _context.Sexos.Any(e => e.ID == id);
        }
    }
}