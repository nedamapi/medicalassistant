﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MedicalAssistant.Datos;
using MedicalAssistant.Entidades;
using MedicalAssistant.Servicios;
using MedicalAssistant.Contracts;
using MedicalAssistant.ViewModels.Pacientes;
using MedicalAssistant.Logica;

namespace MedicalAssistant.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PacientesController : ControllerBase
    {
        private readonly MedicalAssistantDbContext _context;
        private readonly ILoggerServicecs _logger;

        public PacientesController(MedicalAssistantDbContext context, ILoggerServicecs logger)
        {
            _context = context;
            _logger = logger;
        }

        // GET: api/Pacientes
        [HttpGet]
        public async Task<IEnumerable<VerPacientesVM>> GetPacientes()
        {
            var pacientes = await _context.Pacientes.Include(se=>se.Seguros).Include(s=>s.Sexos).ToListAsync();
            return pacientes.Select(p=> new VerPacientesVM
            {
                ID = p.ID,
                Sexo = p.Sexos.Descripcion,
                Seguro = p.Seguros.Descripcion,
                Nombres = p.Nombres,
                Apellidos = p.Apellidos,
                Cedula = p.Cedula,
                NumeroSeguro = p.NumeroSeguro,
                FechaNacimiento = p.FechaNacimiento,
                Edad = p.Edad,
                Telefono = p.Telefono,
                Correo = p.Correo,
                Estado = p.Estado
            });
        }

        // GET: api/Pacientes/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPacientes([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var pacientes = await _context.Pacientes.Include(se=>se.Sexos).Include(s=>s.Seguros).SingleOrDefaultAsync(p=>p.ID == id);

            if (pacientes == null)
            {
                return NotFound();
            }

            return Ok(new VerPacientesVM
            {
                ID = pacientes.ID,
                Sexo = pacientes.Sexos.Descripcion,
                Seguro = pacientes.Seguros.Descripcion,
                Nombres = pacientes.Nombres,
                Apellidos = pacientes.Apellidos,
                Cedula = pacientes.Cedula,
                NumeroSeguro = pacientes.NumeroSeguro,
                FechaNacimiento = pacientes.FechaNacimiento,
                Edad = pacientes.Edad,
                Telefono = pacientes.Telefono,
                Correo = pacientes.Correo,
                Estado = pacientes.Estado
            });
            
        }

        // PUT: api/Pacientes/5
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdatePacientes([FromRoute] int id, [FromBody] UpdatePacienteVM model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != model.ID)
            {
                return BadRequest();
            }

            try
            {
                var pacientes = await _context.Pacientes.SingleOrDefaultAsync(p => p.ID == id);

                DateTime zeroTime = new DateTime(1, 1, 1);
                DateTime a = model.FechaNacimiento;
                DateTime b = DateTime.Now;
                TimeSpan span = b - a;
                int edad = (zeroTime + span).Year - 1;

                pacientes.Nombres = model.Nombres;
                pacientes.Apellidos = model.Apellidos;
                pacientes.SeguroID = model.SeguroID;
                pacientes.SexoID = model.SexoID;
                pacientes.Cedula = model.Cedula;
                pacientes.NumeroSeguro = model.NumeroSeguro;
                pacientes.FechaNacimiento = model.FechaNacimiento;
                pacientes.Edad = edad;
                pacientes.Telefono = model.Telefono;
                pacientes.Correo = model.Correo;
                pacientes.FechaModificacion = DateTime.Now;


                await _context.SaveChangesAsync();
                _context.Entry(model).State = EntityState.Modified;
                _logger.logInfo($"Se actualizó el usuario con el ID: {id}");
            }
            catch (Exception ex)
            {
                if (!PacientesExists(id))
                {
                    _logger.logInfo("Usuario No encontrado");
                    return NotFound();
                }
                else
                {
                    _logger.logError(Errores.MessageExcepcion(ex));
                    return BadRequest();
                }
            }

            return Ok(model);
        }

        // POST: api/Pacientes
        [HttpPost]
        public async Task<IActionResult> InsertPacientes([FromBody] InsertPacienteVM model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var date = DateTime.Now;
            Console.WriteLine(date);

            DateTime zeroTime = new DateTime(1, 1, 1);
            DateTime a = model.FechaNacimiento;
            DateTime b = DateTime.Now;
            TimeSpan span = b - a;
            int edad = (zeroTime + span).Year - 1;

            Pacientes pacientes = new Pacientes()
            {
                ID = 0,
                SeguroID = model.SegurosID,
                SexoID = model.SexosID,
                Nombres = model.Nombres,
                Apellidos = model.Apellidos,
                Cedula = model.Cedula,
                NumeroSeguro = model.NumeroSeguro,
                FechaNacimiento = model.FechaNacimiento,
                Edad = edad,
                Correo = model.Correo,
                Telefono = model.Telefono,
                FechaCreacion = DateTime.Now,
                Estado = true
            };

            _context.Pacientes.Add(pacientes);

            try
            {
                await _context.SaveChangesAsync();
                _logger.logInfo($"El paciente con el ID: {model.ID} fue creado correctamente.");
            }
            catch (Exception ex)
            {
                _logger.logError($"Ha sucedido el siguiente error en el intento de creación de un nuevo paciente: {Errores.MessageExcepcion(ex)}");
                return BadRequest();
            }

            return Ok(model);
        }

        // DELETE: api/Pacientes/DesactivarPacientes/5
        [HttpPost("Action/{id}")]
        public async Task<IActionResult> DesactivarPacientes([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var pacientes = await _context.Pacientes.FindAsync(id);
            if (pacientes == null)
            {
                return NotFound();
            }

            pacientes.Estado = false;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                _logger.logError(Errores.MessageExcepcion(ex));
                return BadRequest();
            }

            return Ok(pacientes);
        }

        [HttpPost("Action/{id}")]
        public async Task<IActionResult> ActivarPacientes([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var pacientes = await _context.Pacientes.FindAsync(id);
            if (pacientes == null)
            {
                return NotFound();
            }

            pacientes.Estado = true;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                _logger.logError(Errores.MessageExcepcion(ex));
                return BadRequest();
            }

            return Ok(pacientes);
        }

        private bool PacientesExists(int id)
        {
            return _context.Pacientes.Any(e => e.ID == id);
        }
    }
}