﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MedicalAssistant.Datos;
using MedicalAssistant.Entidades;
using MedicalAssistant.Contracts;
using MedicalAssistant.ViewModels.Consultas;
using MedicalAssistant.Logica;

namespace MedicalAssistant.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ConsultasController : ControllerBase
    {
        private readonly MedicalAssistantDbContext _context;
        private readonly ILoggerServicecs _logger;

        public ConsultasController(MedicalAssistantDbContext context, ILoggerServicecs logger)
        {
            _context = context;
            _logger = logger;
        }

        // GET: api/Consultas
        [HttpGet]
        public async Task<IEnumerable<VerConsultasVM>> GetConsultas()
        {
            var consultas = await _context.Consultas.Include(x => x.Pacientes).ToListAsync();
            return consultas.Select(x=> new VerConsultasVM 
            {
                IdPaciente = x.IdPaciente,
                Paciente = x.Pacientes.Nombres,
                MotivoConsulta = x.MotivoConsulta,
                Antecedentes = x.Antecedentes,
                Diagnostico = x.Diagnostico,
                Tratamiento = x.Tratamiento,
                FechaConsulta = x.FechaConsulta,
                FechaProximaCita = x.FechaProximaCita
            });
        }

        // GET: api/Consultas/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetConsultas([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var query = await _context.Consultas.Include(x=>x.Pacientes).SingleOrDefaultAsync(x=>x.ID == id);

            if (query == null)
            {
                return NotFound();
            }

            return Ok(new VerConsultasVM 
            {
                IdPaciente = query.IdPaciente,
                Paciente = query.Pacientes.Nombres,
                MotivoConsulta = query.MotivoConsulta,
                Antecedentes = query.Antecedentes,
                Diagnostico = query.Diagnostico,
                Tratamiento = query.Tratamiento,
                FechaConsulta = query.FechaConsulta,
                FechaProximaCita = query.FechaProximaCita
            });
        }

        // PUT: api/Consultas/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutConsultas([FromRoute] int id, [FromBody] UpdateConsultaVM model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != model.ID)
            {
                return BadRequest();
            }

            var query = await _context.Consultas.SingleOrDefaultAsync(x => x.ID == id);

            query.MotivoConsulta = model.MotivoConsulta;
            query.Tratamiento = model.Tratamiento;
            query.Antecedentes = model.Antecedentes;
            query.Diagnostico = model.Diagnostico;
            query.FechaConsulta = model.FechaConsulta;
            query.FechaProximaCita = model.FechaProximaCita;

            try
            {
                await _context.SaveChangesAsync();
                _context.Entry(model).State = EntityState.Modified;
            }
            catch (Exception ex)
            {
                if (!ConsultasExists(id))
                {
                    return NotFound();
                }
                else
                {
                    _logger.logError(Errores.MessageExcepcion(ex));
                    return BadRequest(Errores.MessageExcepcion(ex));
                }
            }

            return Ok();
        }

        // POST: api/Consultas
        [HttpPost]
        public async Task<IActionResult> PostConsultas([FromBody] InsertConsultaVM model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Consultas consultas = new Consultas()
            {
                IdPaciente = model.IdPaciente,
                MotivoConsulta = model.MotivoConsulta,
                Antecedentes = model.Antecedentes,
                Diagnostico = model.Diagnostico,
                Tratamiento = model.Tratamiento,
                FechaConsulta = model.FechaConsulta,
                FechaProximaCita = model.FechaProximaCita
            };

            _context.Consultas.Add(consultas);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                _logger.logError(Errores.MessageExcepcion(ex));
                return BadRequest(Errores.MessageExcepcion(ex));
            }

            return Ok(model);
        }

        // DELETE: api/Consultas/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteConsultas([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var consultas = await _context.Consultas.FindAsync(id);

            if (consultas == null)
            {
                return NotFound();
            }

            _context.Consultas.Remove(consultas);
            await _context.SaveChangesAsync();

            return Ok(consultas);
        }

        private bool ConsultasExists(int id)
        {
            return _context.Consultas.Any(e => e.ID == id);
        }
    }
}