﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MedicalAssistant.Datos;
using MedicalAssistant.Entidades;
using MedicalAssistant.Contracts;
using MedicalAssistant.ViewModels.Seguros;

namespace MedicalAssistant.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SegurosController : ControllerBase
    {
        private readonly MedicalAssistantDbContext _context;
        private readonly ILoggerServicecs _logger; 

        public SegurosController(MedicalAssistantDbContext context, ILoggerServicecs logger)
        {
            _context = context;
            _logger = logger;
        }

        // GET: api/Seguros
        [HttpGet]
        public async Task<IEnumerable<VerSegurosVM>> GetSeguros()
        {
            var seguro = await _context.Seguros.ToListAsync();
            return seguro.Select(x =>new VerSegurosVM 
            { 
                ID = x.ID,
                Descripcion = x.Descripcion
            });
        }

        // GET: api/Seguros/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetSeguros([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var seguros = await _context.Seguros.SingleOrDefaultAsync(x=>x.ID ==id);

            if (seguros == null)
            {
                return NotFound();
            }

            return Ok(new VerSegurosVM {
                ID = seguros.ID,
                Descripcion = seguros.Descripcion
            });
        }

        // PUT: api/Seguros/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSeguros([FromRoute] int id, [FromBody] Seguros seguros)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != seguros.ID)
            {
                return BadRequest();
            }

            _context.Entry(seguros).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SegurosExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Seguros
        [HttpPost]
        public async Task<IActionResult> PostSeguros([FromBody] Seguros seguros)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Seguros.Add(seguros);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSeguros", new { id = seguros.ID }, seguros);
        }

        // DELETE: api/Seguros/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSeguros([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var seguros = await _context.Seguros.FindAsync(id);
            if (seguros == null)
            {
                return NotFound();
            }

            _context.Seguros.Remove(seguros);
            await _context.SaveChangesAsync();

            return Ok(seguros);
        }

        private bool SegurosExists(int id)
        {
            return _context.Seguros.Any(e => e.ID == id);
        }
    }
}