﻿using MedicalAssistant.Entidades;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalAssistant.Mappers
{
    public class SegurosMap : IEntityTypeConfiguration<Seguros>
    {
        public void Configure(EntityTypeBuilder<Seguros> builder)
        {
            builder.ToTable("Seguros").HasKey("ID");

            builder.HasMany(p => p.Pacientes).WithOne(p => p.Seguros);
        }
    }

}
