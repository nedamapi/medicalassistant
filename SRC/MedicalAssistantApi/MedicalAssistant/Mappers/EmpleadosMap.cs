﻿using MedicalAssistant.Entidades;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalAssistant.Mappers
{
    public class EmpleadosMap : IEntityTypeConfiguration<Empleados>
    {
        public void Configure(EntityTypeBuilder<Empleados> builder)
        {
            builder.ToTable("Empleados").HasKey("ID");

            builder.HasOne(e => e.Cargos).WithMany(c => c.Empleados);

        }
    }
}
