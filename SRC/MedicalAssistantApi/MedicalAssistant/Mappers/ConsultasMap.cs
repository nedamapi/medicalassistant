﻿using MedicalAssistant.Entidades;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalAssistant.Mappers
{
    public class ConsultasMap : IEntityTypeConfiguration<Consultas>
    {
        public void Configure(EntityTypeBuilder<Consultas> builder)
        {
            builder.ToTable("Consultas").HasKey("ID");

            builder.HasOne(c => c.Pacientes).WithMany(x=>x.Consultas);
        }
    }
}
