﻿using MedicalAssistant.Entidades;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalAssistant.Mappers
{
    public class PacientesMap : IEntityTypeConfiguration<Pacientes>
    {
        public void Configure(EntityTypeBuilder<Pacientes> builder)
        {
            builder.ToTable("Paciente").HasKey("ID");
            builder.HasOne(p => p.Sexos).WithOne(p => p.Pacientes).HasForeignKey<Sexos>(x => x.ID);
            builder.HasOne(p => p.Seguros).WithMany(p => p.Pacientes);
        }
    }
}
