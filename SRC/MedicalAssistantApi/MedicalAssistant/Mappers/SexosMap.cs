﻿using MedicalAssistant.Entidades;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalAssistant.Mappers
{
    public class SexosMap : IEntityTypeConfiguration<Sexos>
    {
        public void Configure(EntityTypeBuilder<Sexos> builder)
        {
            builder.ToTable("Sexos").HasKey("ID");
            builder.HasOne(s => s.Pacientes).WithOne(p=>p.Sexos);
        }
    }
}
