﻿using MedicalAssistant.Entidades;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalAssistant.Mappers
{
    public class CargosMap : IEntityTypeConfiguration<Cargos>
    {
        public void Configure(EntityTypeBuilder<Cargos> builder )
        {
            builder.ToTable("Cargos").HasKey("ID");

            builder.HasMany(c => c.Empleados).WithOne(c => c.Cargos);
        }
    }
}
