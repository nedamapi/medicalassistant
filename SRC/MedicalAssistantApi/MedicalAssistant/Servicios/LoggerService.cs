﻿using MedicalAssistant.Contracts;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalAssistant.Servicios
{
    public class LoggerService : ILoggerServicecs
    {
        public static NLog.ILogger _logger = LogManager.GetCurrentClassLogger();

        public void logError(string message)
        {
            _logger.Error(message);
        }

        public void logInfo(string message)
        {
            _logger.Info(message);
        }

        public void logWarn(string message)
        {
            _logger.Warn(message);
        }
    }
}
