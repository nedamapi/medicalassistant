﻿using MedicalAssistant.Entidades;
using MedicalAssistant.Mappers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalAssistant.Datos
{
    public class MedicalAssistantDbContext : DbContext
    {
        public DbSet<Sexos> Sexos { get; set; }
        public DbSet<Pacientes> Pacientes { get; set; }
        public DbSet<Consultas> Consultas { get; set; }
        public DbSet<Seguros> Seguros { get; set; }
        public DbSet<Empleados> Empleados{ get; set; }
        public DbSet<Cargos> Cargos { get; set; }


        public MedicalAssistantDbContext(DbContextOptions<MedicalAssistantDbContext> options) : base(options)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new SexosMap());
            modelBuilder.ApplyConfiguration(new PacientesMap());
            modelBuilder.ApplyConfiguration(new ConsultasMap());
            modelBuilder.ApplyConfiguration(new SegurosMap());
            modelBuilder.ApplyConfiguration(new EmpleadosMap());
            modelBuilder.ApplyConfiguration(new CargosMap());

        }

    }
}
